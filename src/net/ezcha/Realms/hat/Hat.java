package net.ezcha.Realms.hat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import net.ezcha.Realms.main.Main;

public class Hat extends HatCommand implements Listener {
	
	//Register that it's a plugin
	Main plugin;
	
	public Hat(Main plugin){
        this.plugin = plugin;  
        plugin.getServer().getPluginManager().registerEvents(this, plugin);   
        start();
	}
	
	ArrayList<Short> ignoreDur = new ArrayList<Short>();
	public void start() {
		ignoreDur.add((short)1);
		ignoreDur.add((short)10);
		ignoreDur.add((short)11);
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event) {
		List <ItemStack> l = event.getDrops();
		Iterator<ItemStack> i = l.iterator();
		while(i.hasNext()){
			ItemStack item = i.next();
			if (item.getType() == Material.DIAMOND_HOE) {
				if (item.getItemMeta().spigot().isUnbreakable()) {
					if (!ignoreDur.contains(item.getDurability())) {
						l.remove(item);
					}
				}
			}
		}
	}
    
	//Events
	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent event) {
		ItemStack dropItem = event.getItemDrop().getItemStack();
		if (dropItem.getType() == Material.DIAMOND_HOE) {
			if (dropItem.getItemMeta().spigot().isUnbreakable()) {
				if (!ignoreDur.contains(dropItem.getDurability())) {
					event.setCancelled(true);
					event.getPlayer().sendMessage(ChatColor.RED + "Use /hats to remove your hat.");
					event.getPlayer().getInventory().setHelmet(event.getItemDrop().getItemStack());
					event.getPlayer().updateInventory();
				}
			}
		}
	}
	
	@EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
	    ItemStack clicked = e.getCurrentItem();
	    Inventory inventory = e.getInventory();
	    
        if (e.getSlotType() == SlotType.ARMOR) {
            if (clicked.getType() == Material.DIAMOND_HOE && clicked.getItemMeta().spigot().isUnbreakable()) {
            	e.setCancelled(true);
                p.sendMessage(ChatColor.RED + "Use /hats to remove your hat.");
                p.updateInventory();
            }
        }
	    
	    if (inventory.getName().equals(menu.getName())) {
	    	if (HatCommand.guiButton.contains(clicked)) {
			    if (clicked.getType() == Material.BARRIER) {
		    		e.setCancelled(true);
		    		p.closeInventory();
		    		if (p.getInventory().getHelmet() != null) {
		    			if (p.getInventory().getHelmet().getType() == Material.DIAMOND_HOE) {
		    				p.getInventory().setHelmet(null);
		    				p.sendMessage(ChatColor.GREEN + "I hope you enjoyed the hat!");
		    			} else {
		    				p.sendMessage(ChatColor.RED + "You are not wearing a hat!");
		    			}
		    		} else {
		    			p.sendMessage(ChatColor.RED + "You are not wearing a hat!");
		    		}
		    	}  else {
			    	e.setCancelled(true);
			    	p.closeInventory();
			    	if (p.getInventory().getHelmet() != null) {
			    		if (p.getInventory().getHelmet().getType() != Material.DIAMOND_HOE) {
			    			Item item = p.getWorld().dropItemNaturally(p.getLocation(), p.getInventory().getHelmet());
	        				item.setItemStack(p.getInventory().getHelmet());
			    		}
			    	}
			    	p.getInventory().setHelmet(new ItemStack(Material.AIR, 1));
			    	//Get item and set it
			    	p.getInventory().setHelmet(clicked);
			        //End
					p.sendMessage(ChatColor.GREEN + "Enjoy your new hat!");
		    	}
	    	}
	    }
	}
}
