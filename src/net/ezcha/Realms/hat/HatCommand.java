package net.ezcha.Realms.hat;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class HatCommand implements CommandExecutor {
	
	static ArrayList<ItemStack> guiButton = new ArrayList<ItemStack>();
	
	///
	///	Create GUIs
	///
	public static Inventory menu = Bukkit.createInventory(null, 9, ChatColor.BLUE+"Hats");
	static {
		
		//Remove
	    ItemStack rem = new ItemStack(Material.BARRIER);
	    ItemMeta remM = rem.getItemMeta();
	    remM.setDisplayName(ChatColor.RED+"Remove Hat");
	    remM.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
	    remM.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
	    rem.setItemMeta(remM);
	    guiButton.add(rem);
		
		//Chef
	    ItemStack chef = new ItemStack(Material.DIAMOND_HOE);
	    ItemMeta chefM = chef.getItemMeta();
	    chef.setDurability((short)(2));
	    chefM.spigot().setUnbreakable(true);
	    chefM.setDisplayName(ChatColor.WHITE+"Chef Hat");
	    chefM.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
	    chefM.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
	    chef.setItemMeta(chefM);
	    guiButton.add(chef);
	    
	    //10-Gallon
	    ItemStack tenG = new ItemStack(Material.DIAMOND_HOE);
	    ItemMeta tenGM = tenG.getItemMeta();
	    tenG.setDurability((short)(3));
	    tenGM.spigot().setUnbreakable(true);
	    tenGM.setDisplayName(ChatColor.WHITE+"Ten Gallon Hat");
	    tenGM.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
	    tenGM.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
	    tenG.setItemMeta(tenGM);
	    guiButton.add(tenG);
	    
	    //groucho
	    ItemStack groucho = new ItemStack(Material.DIAMOND_HOE);
	    ItemMeta grouchoM = groucho.getItemMeta();
	    grouchoM.setDisplayName(ChatColor.WHITE+"Groucho");
	    grouchoM.spigot().setUnbreakable(true);
	    groucho.setDurability((short)(4));
	    grouchoM.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
	    grouchoM.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
	    groucho.setItemMeta(grouchoM);
	    guiButton.add(groucho);
	    
	    //Mohawk
	    ItemStack mohawk = new ItemStack(Material.DIAMOND_HOE);
	    ItemMeta mohawkM = mohawk.getItemMeta();
	    mohawkM.setDisplayName(ChatColor.WHITE+"Mohawk");
	    mohawkM.spigot().setUnbreakable(true);
	    mohawk.setDurability((short)(5));
	    mohawkM.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
	    mohawkM.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
	    mohawk.setItemMeta(mohawkM);
	    guiButton.add(mohawk);
	    
	    //Scarf
	    ItemStack scarf = new ItemStack(Material.DIAMOND_HOE);
	    ItemMeta scarfM = scarf.getItemMeta();
	    scarfM.setDisplayName(ChatColor.WHITE+"Scarf");
	    scarfM.spigot().setUnbreakable(true);
	    scarf.setDurability((short)(6));
	    scarfM.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
	    scarfM.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
	    scarf.setItemMeta(scarfM);
	    guiButton.add(scarf);
	    
	    //Sunglasses
	    ItemStack sung = new ItemStack(Material.DIAMOND_HOE);
	    ItemMeta sungM = sung.getItemMeta();
	    sungM.setDisplayName(ChatColor.WHITE+"Sunglasses");
	    sungM.spigot().setUnbreakable(true);
	    sung.setDurability((short)(7));
	    sungM.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
	    sungM.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
	    sung.setItemMeta(sungM);
	    guiButton.add(sung);
	    
	    //Top Hat
	    ItemStack top = new ItemStack(Material.DIAMOND_HOE);
	    ItemMeta topH = top.getItemMeta();
	    topH.setDisplayName(ChatColor.WHITE+"Top Hat");
	    topH.spigot().setUnbreakable(true);
	    top.setDurability((short)(8));
	    topH.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
	    topH.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
	    top.setItemMeta(topH);
	    guiButton.add(top);
	    
	    //Traffic Cone
	    ItemStack traffic = new ItemStack(Material.DIAMOND_HOE);
	    ItemMeta trafficM = traffic.getItemMeta();
	    trafficM.setDisplayName(ChatColor.WHITE+"Traffic Cone");
	    trafficM.spigot().setUnbreakable(true);
	    traffic.setDurability((short)(9));
	    trafficM.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
	    trafficM.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
	    traffic.setItemMeta(trafficM);
	    guiButton.add(traffic);
	    
        //Set items
        menu.setItem(0, chef);
        menu.setItem(1, tenG);
        menu.setItem(2, groucho);
        menu.setItem(3, mohawk);
        menu.setItem(4, scarf);
        menu.setItem(5, top);
        menu.setItem(6, sung);
        menu.setItem(7, traffic);
        menu.setItem(8, rem);
	        
	}
	
	
	///
	///	Command
	///
	
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
    	
    	if (cmd.getName().equalsIgnoreCase("hat") && sender instanceof Player){
    		
    		Player player = (Player) sender;
    		if (player.hasPermission("realms.hat")){
    			player.openInventory(menu);
			}
    		
    		return true;
    	}
    	return false;
    }
    
    
}