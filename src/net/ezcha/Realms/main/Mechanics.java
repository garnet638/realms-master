package net.ezcha.Realms.main;

import java.util.Random;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.Snowman;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.util.Vector;

public class Mechanics implements Listener {

	public Mechanics(Main plugin) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	//Add enchant checker
	public boolean hasEnchantedItem(Player p, ItemStack i, Enchantment e){
		ItemStack[] inv = p.getInventory().getContents();
		for(ItemStack item:inv){
			if(item.getType().equals(i.getType())){
				if(item.getEnchantments().containsKey(e)){
					return true;
				}
			}
		}
		return false;
	}
	
	
	
	
	//Players
	@EventHandler
		public void RightClick(PlayerInteractEntityEvent e) {
		
	    if (e.getHand() == EquipmentSlot.OFF_HAND) {
	        return; // off hand packet, ignore.
	    }
		
		if (e.getRightClicked().getType() == EntityType.PLAYER) {
			Player v = (Player) e.getRightClicked();
			Player p = e.getPlayer();
			
			p.chat("/meow "+v.getName());
		}
	}
	
	//Enderpearls
	/*
	@EventHandler
	public void onProjectileLaunch(ProjectileLaunchEvent event) {
	    Projectile proj = event.getEntity();
	    if (proj instanceof EnderPearl) {
	        EnderPearl pearl = (EnderPearl)proj;
	        ProjectileSource source = pearl.getShooter();
	        if (source instanceof Player) {
	            Player player = (Player)source;
	            pearl.setPassenger(player);
	        }
	    }
	}
	*/
	
	//Snowball Thing
	@EventHandler(priority = EventPriority.HIGHEST)
	public void snowballDamage(EntityDamageByEntityEvent e){
		if (e.isCancelled()) return;
		if (e.getDamager().getType() == EntityType.SNOWBALL) {
			e.setDamage(2D);
		}
	}
	
	@EventHandler
	public void onProjectileHit(ProjectileHitEvent event) {
		Projectile proj = event.getEntity();
		ProjectileSource owner = proj.getShooter();
		if (proj instanceof Snowball) {
			if (owner instanceof Snowman) {
			} else {
				if (owner instanceof Player) {
					Player player = ((Player) owner).getPlayer();
					if (player.getGameMode() != GameMode.CREATIVE) {
						proj.getWorld().dropItemNaturally(proj.getLocation(), new ItemStack(Material.SNOW_BALL));
						proj.getWorld().playSound(proj.getLocation(), Sound.BLOCK_SNOW_HIT, 2, 1);
					}
				} else {
					proj.getWorld().dropItemNaturally(proj.getLocation(), new ItemStack(Material.SNOW_BALL));
					proj.getWorld().playSound(proj.getLocation(), Sound.BLOCK_SNOW_HIT, 2, 1);
				}
			}
		}
		if (proj instanceof EnderPearl) {
			proj.getWorld().playSound(proj.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 1, 1);
		}
	}
	/*
	@EventHandler
	public void onPearlLand(PlayerTeleportEvent event) {
		if (event.getCause() == TeleportCause.ENDER_PEARL) {
			event.setCancelled(true);
		}
	}
	*/
	
	//Playerheads
	@EventHandler
    public void anyName(PlayerDeathEvent event) {
        Player p = event.getEntity();
        if(p.isDead()) {
            p.getKiller();
            if(p.getKiller() instanceof Player) {
            	Random rand = new Random();
            	int number = rand.nextInt(100) + 1;
            	if(number <= 25) {
            		@SuppressWarnings("deprecation")
            		ItemStack skull = new ItemStack(397, 1, (short) 3);
            		SkullMeta meta = (SkullMeta) skull.getItemMeta();
            		meta.setOwner(p.getName());
            		skull.setItemMeta(meta);
            		Item item = p.getWorld().dropItemNaturally(p.getLocation(), skull);
            		item.setItemStack(skull);
            	}
            }
        }
    }
	
	//Glider
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		
		if (player.getInventory().getChestplate() != null) {
		    if (player.getInventory().getChestplate().containsEnchantment(Enchantment.ARROW_INFINITE)) {
				if (player.isGliding()) {
				Location blockBelow0 = player.getLocation().subtract(0, 1, 0);
				Location blockBelow1 = player.getLocation().subtract(0, 2, 0);
				Location blockBelow2 = player.getLocation().subtract(0, 3, 0);
				Location blockBelow3 = player.getLocation().subtract(0, 4, 0);
				Location blockBelow4 = player.getLocation().subtract(0, 5, 0);
				if(blockBelow0.getBlock().getType() == Material.FIRE || blockBelow1.getBlock().getType() == Material.FIRE || blockBelow2.getBlock().getType() == Material.FIRE || blockBelow3.getBlock().getType() == Material.FIRE || blockBelow4.getBlock().getType() == Material.FIRE) {
	    			player.playSound(player.getLocation(), Sound.ENTITY_BLAZE_SHOOT, 1, 1);
	    			player.setVelocity(player.getVelocity().setY(0));
	    			player.setVelocity(player.getVelocity().multiply(new Vector (1.05,0,1.05)));
	    			player.setVelocity(player.getVelocity().add(new Vector (0,1,0)));
	            	Random rand = new Random();
	            	int number = rand.nextInt(100) + 1;
	            	if(number <= 40) {
	            		ItemStack elytra = player.getInventory().getChestplate();
	            		elytra.setDurability((short) (elytra.getDurability() + 2));
	            	}
	    		}
				if(blockBelow0.getBlock().getType() == Material.LAVA || blockBelow1.getBlock().getType() == Material.LAVA || blockBelow2.getBlock().getType() == Material.LAVA || blockBelow3.getBlock().getType() == Material.LAVA || blockBelow4.getBlock().getType() == Material.LAVA) {
	    			player.playSound(player.getLocation(), Sound.ENTITY_BLAZE_SHOOT, 1, 1);
	    			player.setVelocity(player.getVelocity().setY(0));
	    			player.setVelocity(player.getVelocity().multiply(new Vector (1.05,0,1.05)));
	    			player.setVelocity(player.getVelocity().add(new Vector (0,0.8,0)));
	            	Random rand = new Random();
	            	int number = rand.nextInt(100) + 1;
	            	if(number <= 40) {
	            		ItemStack elytra = player.getInventory().getChestplate();
	            		elytra.setDurability((short) (elytra.getDurability() + 2));
	            	}
	    		}
				if(blockBelow0.getBlock().getType() == Material.STATIONARY_LAVA || blockBelow1.getBlock().getType() == Material.STATIONARY_LAVA || blockBelow2.getBlock().getType() == Material.STATIONARY_LAVA || blockBelow3.getBlock().getType() == Material.STATIONARY_LAVA || blockBelow4.getBlock().getType() == Material.STATIONARY_LAVA) {
	    			player.playSound(player.getLocation(), Sound.ENTITY_BLAZE_SHOOT, 1, 1);
	    			player.setVelocity(player.getVelocity().setY(0));
	    			player.setVelocity(player.getVelocity().multiply(new Vector (1.05,0,1.05)));
	    			player.setVelocity(player.getVelocity().add(new Vector (0,0.8,0)));
	            	Random rand = new Random();
	            	int number = rand.nextInt(100) + 1;
	            	if(number <= 40) {
	            		ItemStack elytra = player.getInventory().getChestplate();
	            		elytra.setDurability((short) (elytra.getDurability() + 2));
	            	}
	    		}
			}
				
//				//Infinity Elytras
//				if(hasEnchantedItem(player, new ItemStack(Material.ELYTRA), Enchantment.ARROW_INFINITE)){
//				{
//	    			player.setVelocity(player.getVelocity().setY(0));
//	    			player.setVelocity(player.getVelocity().multiply(new Vector (1.05,0,1.05)));
//	    			//player.setVelocity(player.getVelocity().add(new Vector (0,0.8,0)));
//	            	}
//	    		}
				

	    	}
	    }
	//}
	}
	
}
