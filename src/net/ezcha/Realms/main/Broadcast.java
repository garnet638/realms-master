package net.ezcha.Realms.main;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

public class Broadcast implements Listener {

	public static List<String> ignoreList = new ArrayList<String>();
	
	public static int mesNum = 0;
	public static String message = "";

	public Broadcast(Main plugin) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		start(plugin);
	}
	
	public void start(Plugin plugin) {
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
			public void run() {
				String chatTemplate = ChatColor.DARK_PURPLE+"["+ChatColor.LIGHT_PURPLE+"Info"+ChatColor.DARK_PURPLE+"] "+ChatColor.WHITE;
				
				if (mesNum == 0) {
					message = "Make sure to go through "+ChatColor.BLUE+"/warp tutorial"+ChatColor.WHITE+" if you haven't!";
				}
				if (mesNum == 1) {
					message = "Pay to win sucks, so donators get cosmetics. Go to "+ChatColor.BLUE+"/warp donate"+ChatColor.WHITE+" for more info.";
				}
				if (mesNum == 2) {
					message = "Don't forget to check the rules at "+ChatColor.BLUE+"/warp rules"+ChatColor.WHITE+"!";
				}
				if (mesNum == 3) {
					message = "You can see all the staff members and contact them with "+ChatColor.BLUE+"/staff"+ChatColor.WHITE+"!";
				}
				if (mesNum == 4) {
					message = "Is your friend AFK and you need their attention? Use "+ChatColor.BLUE+"/meow <Username>"+ChatColor.WHITE+" to get it!";
				}
				if (mesNum == 5) {
					message = "Are these info messages getting annoying? Toggle them with "+ChatColor.BLUE+"/info"+ChatColor.WHITE+"!";
				}
				if (mesNum == 6) {
					message = "Make sure to check out our wiki at "+ChatColor.BLUE+"http://wiki.mirealms.com"+ChatColor.WHITE+"!";
				}
				if (mesNum == 7) {
					message = "Climbing has been temporarily disabled due to lag.";
					mesNum = -1;
				}
				
				for (Player player : Bukkit.getOnlinePlayers()) {
					if (!ignoreList.contains(player.getUniqueId().toString())) {
						player.playSound(player.getLocation(), Sound.BLOCK_NOTE_HAT, 1, 1);
						player.sendMessage(chatTemplate+message);
					}
				 }
				
				mesNum += 1;
			}
		}, 0L, 20L*300);
	}
	
}
