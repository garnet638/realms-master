package net.ezcha.Realms.main;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import net.ezcha.Realms.commands.AdminChat;
import net.ezcha.Realms.commands.Bank;
import net.ezcha.Realms.commands.Booster;
import net.ezcha.Realms.commands.Butt;
import net.ezcha.Realms.commands.ConsoleChat;
import net.ezcha.Realms.commands.FakeJoin;
import net.ezcha.Realms.commands.FakeLeave;
import net.ezcha.Realms.commands.HelperChat;
import net.ezcha.Realms.commands.Id;
import net.ezcha.Realms.commands.LMGTFY;
import net.ezcha.Realms.commands.Sudo;
import net.ezcha.Realms.commands.ToggleInfo;
import net.ezcha.Realms.customItems.CraftingBlock;
import net.ezcha.Realms.customItems.Recipes;
import net.ezcha.Realms.customItems.SmokeBomb;
import net.ezcha.Realms.customItems.Vial;
import net.ezcha.Realms.hat.Hat;
import net.ezcha.Realms.hat.HatCommand;
import net.ezcha.Realms.particles.ParticleCommand;
import net.ezcha.Realms.particles.SetParticle;
import net.ezcha.Realms.staff.Staff;
import net.ezcha.Realms.staff.StaffCommand;
import net.milkbowl.vault.economy.Economy;

public class Main extends JavaPlugin {

	public static Economy econ = null;
	
	public static Main instance;
	
	@Override
	public void onEnable() {
		Main.instance = this;
		
		if (!setupEconomy()) {
			getLogger().info("Vault not found, Realms plugin disabled.");
			getServer().getPluginManager().disablePlugin(this);
		}
		
		getLogger().info("Realms plugin by Ezcha and Garnet started.");
		
		getCommand("butt").setExecutor(new Butt());
		getCommand("lmgtfy").setExecutor(new LMGTFY());
		
		getCommand("hat").setExecutor(new HatCommand());
		getCommand("hats").setExecutor(new HatCommand());
		new Hat(this);
		
		new SmokeBomb(this);
		CraftingBlock crafter = new CraftingBlock(this);
		getCommand("crafter").setExecutor(crafter);
		Vial vial = new Vial(this);
		getCommand("vial").setExecutor(vial);
		
		Bank ATM = new Bank(this);
		getCommand("bank").setExecutor(ATM);
		
		getCommand("particles").setExecutor(new ParticleCommand());
		new SetParticle(this);
		
		getCommand("staff").setExecutor(new StaffCommand());
		new Staff(this);
		
		getCommand("info").setExecutor(new ToggleInfo());
		
		
		getCommand("boost").setExecutor(new Booster());
		new BoostEffects(this);
		
		new Mechanics(this);
		
		new Broadcast(this);
		
		getCommand("fakejoin").setExecutor(new FakeJoin());
		getCommand("fj").setExecutor(new FakeJoin());
		
		getCommand("fakeleave").setExecutor(new FakeLeave());
		getCommand("fl").setExecutor(new FakeLeave());
		
		getCommand("a").setExecutor(new AdminChat());
		
		getCommand("h").setExecutor(new HelperChat());
		
		getCommand("sc").setExecutor(new ConsoleChat());
		
		//Commands
		getCommand("sudo").setExecutor(new Sudo());
		getCommand("id").setExecutor(new Id());
		
		new Recipes(this);
		
		//Config
		loadConfig();
	}
	
	@Override
	public void onDisable() {
		saveConfigData();
	}
	
	private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }
	
	public void loadConfig() {
		getConfig().options().copyDefaults(true);
		saveConfig();
		if (getConfig().getStringList("ignore-list") != null) {
			Broadcast.ignoreList = getConfig().getStringList("ignore-list");
		}
		if (getConfig().getStringList("craft-list") != null) {
			CraftingBlock.craftList = getConfig().getStringList("craft-list");
		}
		if (getConfig().getStringList("vial-list") != null) {
			Vial.vialList = getConfig().getStringList("vial-list");
		}
		Set<String> shops = getConfig().getConfigurationSection("shop-list").getKeys(false);
		for(String key : shops) {
		    Bank.addShop(key, getConfig().getString("shop-list."+key+".warp"), Material.valueOf(getConfig().getString("shop-list."+key+".icon")));
		}
	}
	
	
	public void saveConfigData() {
		getConfig().set("ignore-list", Broadcast.ignoreList);
		getConfig().set("craft-list", CraftingBlock.craftList);
		getConfig().set("vial-list", Vial.vialList);
		getConfig().set("shop-list", null);
		List<HashMap<String, String>> shopsList = Bank.shopsList;
		for (int i = 0; i < shopsList.size(); i++) {
			HashMap<String, String> map = shopsList.get(i);
			String name = map.get("Name");
			String warp = map.get("Warp");
			String icon = map.get("Icon").toString();
			getConfig().set("shop-list."+name+".warp", warp);
			getConfig().set("shop-list."+name+".icon", icon);
		}
		saveConfig();
	}
	
}