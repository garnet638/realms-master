package net.ezcha.Realms.staff;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

@SuppressWarnings("deprecation")
public class StaffCommand implements CommandExecutor {
	
	static List<String> owner = Arrays.asList(ChatColor.RESET+""+ChatColor.BLUE+"Owner");
	static List<String> moderator = Arrays.asList(ChatColor.RESET+""+ChatColor.BLUE+"Moderator");
	static List<String> nerd = Arrays.asList(ChatColor.RESET+""+ChatColor.BLUE+"Nerd/Moderator");
	static List<String> builder = Arrays.asList(ChatColor.RESET+""+ChatColor.BLUE+"Builder/Helper");
	static List<String> buildern = Arrays.asList(ChatColor.RESET+""+ChatColor.BLUE+"Builder");
	static List<String> builderm = Arrays.asList(ChatColor.RESET+""+ChatColor.BLUE+"Builder/Moderator");
	static List<String> developer = Arrays.asList(ChatColor.RESET+""+ChatColor.BLUE+"Developer/Moderator");
	static List<String> gdf = Arrays.asList(ChatColor.RESET+""+ChatColor.BLUE+"Green Day Fanatic/Helper");
	static List<String> helper = Arrays.asList(ChatColor.RESET+""+ChatColor.BLUE+"Helper");
	
	///
	///	Create GUIs
	///
	public static Inventory staffMenu = Bukkit.createInventory(null, 45, ChatColor.BLUE+"Staff");
	static {
		
		///
		///Ranks
		///
		//Owner
	    ItemStack ownerp = new ItemStack(160, 1, (short) 2);
		ItemMeta ownerMeta = ownerp.getItemMeta();
		ownerMeta.setDisplayName(ChatColor.WHITE+"Owners");
		ownerp.setItemMeta(ownerMeta);
		//builder
	    ItemStack builderp = new ItemStack(160, 1, (short) 14);
	    ItemMeta builderMeta = builderp.getItemMeta();
		builderMeta.setDisplayName(ChatColor.WHITE+"Builders");
		builderp.setItemMeta(builderMeta);
		//mod
	    ItemStack modp = new ItemStack(160, 1, (short) 11);
	    ItemMeta modMeta = modp.getItemMeta();
		modMeta.setDisplayName(ChatColor.WHITE+"Moderators");
		modp.setItemMeta(modMeta);
		//helper
	    ItemStack helperp = new ItemStack(160, 1, (short) 5);
	    ItemMeta helperMeta = helperp.getItemMeta();
		helperMeta.setDisplayName(ChatColor.WHITE+"Helpers");
		helperp.setItemMeta(helperMeta);
		
		///
		/// Players
		///
		
		//Ezcha
	    ItemStack ezchaHead = new ItemStack(397, 1, (short) 3);
		SkullMeta ezchaMeta = (SkullMeta) ezchaHead.getItemMeta();
		ezchaMeta.setOwner("Ezcha");
		ezchaMeta.setDisplayName(ChatColor.WHITE+"Ezcha");
		ezchaMeta.setLore(owner);
		ezchaHead.setItemMeta(ezchaMeta);
		
		//Voxy
	    ItemStack VoxyHead = new ItemStack(397, 1, (short) 3);
		SkullMeta VoxyMeta = (SkullMeta) VoxyHead.getItemMeta();
		VoxyMeta.setOwner("Voxolotl");
		VoxyMeta.setDisplayName(ChatColor.WHITE+"Voxy");
		VoxyMeta.setLore(owner);
		VoxyHead.setItemMeta(VoxyMeta);
		
		//vash
	    ItemStack vashHead = new ItemStack(397, 1, (short) 3);
		SkullMeta vashMeta = (SkullMeta) vashHead.getItemMeta();
		vashMeta.setOwner("vash0110");
		vashMeta.setDisplayName(ChatColor.WHITE+"vash0110");
		vashMeta.setLore(buildern);
		vashHead.setItemMeta(vashMeta);
	    
		//Nerd
	    ItemStack nerdHead = new ItemStack(397, 1, (short) 3);
		SkullMeta nerdMeta = (SkullMeta) nerdHead.getItemMeta();
		nerdMeta.setOwner("blockerlocker");
		nerdMeta.setDisplayName(ChatColor.WHITE+"blockerlocker");
		nerdMeta.setLore(nerd);
		nerdHead.setItemMeta(nerdMeta);
		
		//Blox
	    ItemStack bloxHead = new ItemStack(397, 1, (short) 3);
		SkullMeta bloxMeta = (SkullMeta) bloxHead.getItemMeta();
		bloxMeta.setOwner("BloxxerDood");
		bloxMeta.setDisplayName(ChatColor.WHITE+"BloxTheRigger");
		bloxMeta.setLore(builderm);
		bloxHead.setItemMeta(bloxMeta);
		
		//Pika
	    ItemStack pikaHead = new ItemStack(397, 1, (short) 3);
		SkullMeta pikaMeta = (SkullMeta) pikaHead.getItemMeta();
		pikaMeta.setOwner("PikaMasterzMC");
		pikaMeta.setDisplayName(ChatColor.WHITE+"PikaMasterzMC");
		pikaMeta.setLore(builderm);
		pikaHead.setItemMeta(pikaMeta);
		
		//Subject
	    ItemStack subjectHead = new ItemStack(397, 1, (short) 3);
		SkullMeta subjectMeta = (SkullMeta) subjectHead.getItemMeta();
		subjectMeta.setOwner("Kadva_Nox");
		subjectMeta.setDisplayName(ChatColor.WHITE+"Subject_052");
		subjectMeta.setLore(builder);
		subjectHead.setItemMeta(subjectMeta);
		
		//Garnet
	    ItemStack garnetHead = new ItemStack(397, 1, (short) 3);
		SkullMeta garnetMeta = (SkullMeta) garnetHead.getItemMeta();
		garnetMeta.setOwner("Garnet638");
		garnetMeta.setDisplayName(ChatColor.WHITE+"Garnet638");
		garnetMeta.setLore(developer);
		garnetHead.setItemMeta(garnetMeta);
		
		//Mustard
	    ItemStack mustardHead = new ItemStack(397, 1, (short) 3);
		SkullMeta mustardMeta = (SkullMeta) mustardHead.getItemMeta();
		mustardMeta.setOwner("MustardBlock");
		mustardMeta.setDisplayName(ChatColor.WHITE+"MustardBlock");
		mustardMeta.setLore(helper);
		mustardHead.setItemMeta(mustardMeta);
		
		//dono
	    ItemStack donoHead = new ItemStack(397, 1, (short) 3);
		SkullMeta donoMeta = (SkullMeta) donoHead.getItemMeta();
		donoMeta.setOwner("DonoTheGreat");
		donoMeta.setDisplayName(ChatColor.WHITE+"DonoTheGreat");
		donoMeta.setLore(helper);
		donoHead.setItemMeta(donoMeta);
		
		//aro
	    ItemStack aroHead = new ItemStack(397, 1, (short) 3);
		SkullMeta aroMeta = (SkullMeta) aroHead.getItemMeta();
		aroMeta.setOwner("Aronan");
		aroMeta.setDisplayName(ChatColor.WHITE+"Aronan");
		aroMeta.setLore(gdf);
		aroHead.setItemMeta(aroMeta);
		
		//moo
	    ItemStack mooHead = new ItemStack(397, 1, (short) 3);
		SkullMeta mooMeta = (SkullMeta) mooHead.getItemMeta();
		mooMeta.setOwner("Mooshim");
		mooMeta.setDisplayName(ChatColor.WHITE+"Mooshim");
		mooMeta.setLore(helper);
		mooHead.setItemMeta(mooMeta);
		
		//jj
	    ItemStack jjHead = new ItemStack(397, 1, (short) 3);
		SkullMeta jjMeta = (SkullMeta) jjHead.getItemMeta();
		jjMeta.setOwner("Jj052");
		jjMeta.setDisplayName(ChatColor.WHITE+"Jj052");
		jjMeta.setLore(helper);
		jjHead.setItemMeta(jjMeta);
		
		//cade
	    ItemStack cadeHead = new ItemStack(397, 1, (short) 3);
		SkullMeta cadeMeta = (SkullMeta) cadeHead.getItemMeta();
		cadeMeta.setOwner("xTTJ");
		cadeMeta.setDisplayName(ChatColor.WHITE+"xTTJ");
		cadeMeta.setLore(helper);
		cadeHead.setItemMeta(cadeMeta);
		
		//nico
	    ItemStack nicoHead = new ItemStack(397, 1, (short) 3);
		SkullMeta nicoMeta = (SkullMeta) nicoHead.getItemMeta();
		nicoMeta.setOwner("Nicolasev");
		nicoMeta.setDisplayName(ChatColor.WHITE+"Nicolasev");
		nicoMeta.setLore(helper);
		nicoHead.setItemMeta(nicoMeta);
		
		//marty
	    ItemStack martyHead = new ItemStack(397, 1, (short) 3);
		SkullMeta martyMeta = (SkullMeta) martyHead.getItemMeta();
		martyMeta.setOwner("TimeTraveler1985");
		martyMeta.setDisplayName(ChatColor.WHITE+"Marty");
		martyMeta.setLore(helper);
		martyHead.setItemMeta(martyMeta);
		
        //Set items
		staffMenu.setItem(0, ownerp);
	    staffMenu.setItem(1, ezchaHead);
	    staffMenu.setItem(2, VoxyHead);
	    staffMenu.setItem(9, modp);
	    staffMenu.setItem(10, nerdHead);
	    staffMenu.setItem(11, garnetHead);
	    staffMenu.setItem(12, bloxHead);
	    staffMenu.setItem(13, pikaHead);
	    staffMenu.setItem(18, builderp);
	    staffMenu.setItem(19, subjectHead);  
	    staffMenu.setItem(20, vashHead);
	    staffMenu.setItem(21, cadeHead);
	    staffMenu.setItem(27, helperp);
	    staffMenu.setItem(28, mustardHead);
	    staffMenu.setItem(29, donoHead);
	    staffMenu.setItem(30, aroHead);
	    staffMenu.setItem(31, mooHead);
	    staffMenu.setItem(32, jjHead);
	    staffMenu.setItem(31, martyHead);
	    staffMenu.setItem(32, nicoHead);
	    staffMenu.setItem(36, helperp);
	}
	
	
	///
	///	Command
	///
	
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
    	
    	if (cmd.getName().equalsIgnoreCase("staff") && sender instanceof Player){
    		
    		Player player = (Player) sender;
    		player.openInventory(staffMenu);
    		
    		return true;
    	}
    	return false;
    }
    
    
}