package net.ezcha.Realms.staff;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import net.ezcha.Realms.main.Main;

public class Staff extends StaffCommand implements Listener {
	
	//Register that it's a plugin
	Main plugin;
	
	public Server thisPlugin() {
		Main plugin = Main.instance;
		return plugin.getServer();
	}
	
	public Staff(Main plugin){
        this.plugin = plugin;  
        plugin.getServer().getPluginManager().registerEvents(this, plugin);    
	}
    
	//Events
	@EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
	    ItemStack clicked = e.getCurrentItem();
	    Inventory inventory = e.getInventory();
	    
	    if (!(inventory instanceof CraftingInventory)) {
		    if (inventory.getName().equals(staffMenu.getName())) {
		    	e.setCancelled(true);
		    	if (clicked.getType() == Material.SKULL_ITEM) {
			    	
					p.closeInventory();
			    	
			    	SkullMeta meta = (SkullMeta) clicked.getItemMeta();
		    		String staffName = meta.getOwner();
			    	
			    	Player staffDood = thisPlugin().getPlayer(staffName);
			    	String chatTemplate = ChatColor.DARK_PURPLE+"["+ChatColor.LIGHT_PURPLE+"Staff"+ChatColor.DARK_PURPLE+"] "+ChatColor.WHITE;
					if (staffDood == null) {
						p.sendMessage(chatTemplate+"Contact the staff at "+ChatColor.BLUE+"http://www.mirealms.com/contact");
					} else {
						p.sendMessage(chatTemplate+"You can message "+staffName+" with "+ChatColor.BLUE+"/msg "+staffName+ChatColor.WHITE+".");
					}
		    	}
		    }
	    
	    }
	}
}
