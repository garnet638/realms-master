package net.ezcha.Realms.particles;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import net.ezcha.Realms.main.Main;

public class SetParticle extends ParticleCommand implements Listener {
	
	public static Server thisServer = null;
	
	//Register that it's a plugin
	Main plugin;
	
	public SetParticle(Main plugin){
		thisServer = plugin.getServer();
        this.plugin = plugin;  
        plugin.getServer().getPluginManager().registerEvents(this, plugin);    
	}
	
	@EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
	    ItemStack clicked = e.getCurrentItem();
	    Inventory inventory = e.getInventory();
	    
	    if (inventory.getName().equals(ChatColor.BLUE+"Particles")) {
	    	if (ParticleCommand.guiButton.contains(clicked)) {
			    if (clicked.getType() == Material.BARRIER) {
		    		e.setCancelled(true);
		    		p.closeInventory();
		    		thisServer.dispatchCommand(thisServer.getConsoleSender(), "scoreboard players set "+p.getName()+" PARTICLE 0");
		    		p.sendMessage(ChatColor.GREEN + "Particles disabled.");
		    	}
			    
			    if (clicked.getType() == Material.FIREBALL) {
		    		e.setCancelled(true);
		    		p.closeInventory();
		    		thisServer.dispatchCommand(thisServer.getConsoleSender(), "scoreboard players set "+p.getName()+" PARTICLE 1");
		    		p.sendMessage(ChatColor.GREEN + "Toasty!");
		    	}
			    
			    if (clicked.getType() == Material.RED_ROSE) {
		    		e.setCancelled(true);
		    		p.closeInventory();
		    		thisServer.dispatchCommand(thisServer.getConsoleSender(), "scoreboard players set "+p.getName()+" PARTICLE 2");
		    		p.sendMessage(ChatColor.GREEN + "bby luv me 4evr?");
		    	}
			    
			    if (clicked.getType() == Material.INK_SACK) {
		    		e.setCancelled(true);
		    		p.closeInventory();
		    		thisServer.dispatchCommand(thisServer.getConsoleSender(), "scoreboard players set "+p.getName()+" PARTICLE 3");
		    		p.sendMessage(ChatColor.GREEN + "Is the old man really snoring?");
		    	}
			    
			    if (clicked.getType() == Material.DIAMOND) {
		    		e.setCancelled(true);
		    		p.closeInventory();
		    		thisServer.dispatchCommand(thisServer.getConsoleSender(), "scoreboard players set "+p.getName()+" PARTICLE 4");
		    		p.sendMessage(ChatColor.GREEN + "Can I have a small loan of a million dollars?");
		    	}
			    
			    if (clicked.getType() == Material.OBSIDIAN) {
		    		e.setCancelled(true);
		    		p.closeInventory();
		    		thisServer.dispatchCommand(thisServer.getConsoleSender(), "scoreboard players set "+p.getName()+" PARTICLE 5");
		    		p.sendMessage(ChatColor.GREEN + "Smaller on the inside?");
		    	}
			    
			    if (clicked.getType() == Material.SKULL_ITEM) {
		    		e.setCancelled(true);
		    		p.closeInventory();
		    		thisServer.dispatchCommand(thisServer.getConsoleSender(), "scoreboard players set "+p.getName()+" PARTICLE 6");
		    		p.sendMessage(ChatColor.GREEN + "Enderboy? xX_Enderbrine_Xx? I don't know, some sort of cool and edgy name.");
		    	}
			    
			    if (clicked.getType() == Material.ENCHANTMENT_TABLE) {
		    		e.setCancelled(true);
		    		p.closeInventory();
		    		thisServer.dispatchCommand(thisServer.getConsoleSender(), "scoreboard players set "+p.getName()+" PARTICLE 7");
		    		p.sendMessage(ChatColor.GREEN + "Maybe you can enchant yourself a life.");
		    	}
			    
			    if (clicked.getType() == Material.END_CRYSTAL) {
		    		e.setCancelled(true);
		    		p.closeInventory();
		    		thisServer.dispatchCommand(thisServer.getConsoleSender(), "scoreboard players set "+p.getName()+" PARTICLE 8");
		    		p.sendMessage(ChatColor.GREEN + "Good luck getting these out anytime soon.");
		    	}
			    
		    }
		}
	}
}
