package net.ezcha.Realms.particles;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ParticleCommand implements CommandExecutor {
	
	static ArrayList<ItemStack> guiButton = new ArrayList<ItemStack>();
	
	///
	///	Create GUIs
	///
	public static Inventory particlemenu = Bukkit.createInventory(null, 9, ChatColor.BLUE+"Particles");
	static {
		
		//Remove
	    ItemStack rem = new ItemStack(Material.BARRIER);
	    ItemMeta remM = rem.getItemMeta();
	    remM.setDisplayName(ChatColor.RED+"Remove Particles");
	    rem.setItemMeta(remM);
	    guiButton.add(rem);
		
		//Fire
	    ItemStack flame = new ItemStack(Material.FIREBALL);
	    ItemMeta flameM = flame.getItemMeta();
	    flameM.setDisplayName(ChatColor.WHITE+"Burning");
	    flame.setItemMeta(flameM);
	    guiButton.add(flame);
	    
	    //Hearts
	    ItemStack love = new ItemStack(Material.RED_ROSE);
	    ItemMeta loveM = love.getItemMeta();
	    love.setDurability((short)(4));
	    loveM.setDisplayName(ChatColor.WHITE+"Love");
	    love.setItemMeta(loveM);
	    guiButton.add(love);
	    
	    //Rain
	    ItemStack rain = new ItemStack(Material.INK_SACK);
	    ItemMeta rainM = rain.getItemMeta();
	    rain.setDurability((short)(4));
	    rainM.setDisplayName(ChatColor.WHITE+"Rain Cloud");
	    rain.setItemMeta(rainM);
	    guiButton.add(rain);
	    
	    //Money Feet
	    ItemStack colors = new ItemStack(Material.DIAMOND);
	    ItemMeta colorsM = colors.getItemMeta();
	    colorsM.setDisplayName(ChatColor.WHITE+"Money Feet");
	    colors.setItemMeta(colorsM);
	    guiButton.add(colors);
	    
	    //Portal
	    ItemStack portal = new ItemStack(Material.OBSIDIAN);
	    ItemMeta portalM = portal.getItemMeta();
	    portalM.setDisplayName(ChatColor.WHITE+"Portal");
	    portal.setItemMeta(portalM);
	    guiButton.add(portal);
	    
	    //Enderman
	    ItemStack enderman = new ItemStack(Material.SKULL_ITEM);
	    ItemMeta endermanM = enderman.getItemMeta();
	    enderman.setDurability((short)(5));
	    endermanM.setDisplayName(ChatColor.WHITE+"Dragon");
	    enderman.setItemMeta(endermanM);
	    guiButton.add(enderman);
	    
	    //Enchantment
	    ItemStack enchantment = new ItemStack(Material.ENCHANTMENT_TABLE);
	    ItemMeta enchantmentM = enchantment.getItemMeta();
	    enchantmentM.setDisplayName(ChatColor.WHITE+"Enchantment");
	    enchantment.setItemMeta(enchantmentM);
	    guiButton.add(enchantment);
	    
	    //Crystals
	    ItemStack crystals = new ItemStack(Material.END_CRYSTAL);
	    ItemMeta crystalsM = crystals.getItemMeta();
	    crystalsM.setDisplayName(ChatColor.WHITE+"Glitter");
	    crystals.setItemMeta(crystalsM);
	    guiButton.add(crystals);
	    
        //Set items
	    particlemenu.setItem(0, flame);
        particlemenu.setItem(1, love);
        particlemenu.setItem(2, rain);
        particlemenu.setItem(3, colors);
        particlemenu.setItem(4, portal);
        particlemenu.setItem(5, enderman);
        particlemenu.setItem(6, enchantment);
        particlemenu.setItem(7, crystals);
        particlemenu.setItem(8, rem);
        
	        
	}
	
	
	///
	///	Command
	///
	
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
    	
    	if (cmd.getName().equalsIgnoreCase("particles") && sender instanceof Player){
    		
    		Player player = (Player) sender;
    		if (player.hasPermission("realms.particles")){
    			player.openInventory(particlemenu);
			}
    		
    		return true;
    	}
    	return false;
    }
    
    
}