package net.ezcha.Realms.customItems;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

import net.ezcha.Realms.main.Main;

public class CraftingBlock implements Listener, CommandExecutor {
	
	public static Server thisServer = null;
	public static Plugin thisPlugin = null;
	public static List<String> craftList = new ArrayList<String>();
	
	public static ItemStack createTexture() {
		ItemStack craftingTexture = new ItemStack(Material.DIAMOND_HOE);
		ItemMeta textureMeta = craftingTexture.getItemMeta();
		textureMeta.spigot().setUnbreakable(true);
		textureMeta.setDisplayName(ChatColor.WHITE+"Realms Workbench");
		textureMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		textureMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
		craftingTexture.setItemMeta(textureMeta);
		craftingTexture.setDurability((short)1);
		return craftingTexture;
	}
	
	public CraftingBlock(Main plugin) {
		thisServer = plugin.getServer();
		thisPlugin = plugin;
		thisServer.getPluginManager().registerEvents(this, plugin);
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			Player player = event.getPlayer();
			Block block = event.getClickedBlock();
			ItemStack heldItem = player.getInventory().getItemInMainHand();
			if (block.getType() == Material.DROPPER) {
				if (craftList.contains(block.getLocation().toString())) {
					event.setCancelled(true);
					player.openWorkbench(null, true);
				}
			}
			if (heldItem.getType() == Material.DIAMOND_HOE) {
				
				ItemStack craftingTexture = createTexture();
				
				if (heldItem.isSimilar(craftingTexture)) {
					event.setCancelled(true);
					Block blockSide = block.getRelative(event.getBlockFace());
					if (blockSide.getType() == Material.AIR) {
						if (player.getGameMode() != GameMode.CREATIVE) {
							player.getInventory().removeItem(heldItem);
						}
						
						blockSide.getWorld().playSound(player.getLocation(), "realmsbench.place", 1, 1);
						
						Block newBlock = blockSide.getWorld().getBlockAt(blockSide.getLocation());
						newBlock.setType(Material.DROPPER);
						newBlock.setData((byte)9);
						
						Block aboveBlock = newBlock.getRelative(BlockFace.UP);
						if (aboveBlock.getType() == Material.AIR) {
							ArmorStand armorStand = blockSide.getWorld().spawn(blockSide.getLocation().add(new Vector(0.5,0.1,0.5)), ArmorStand.class);
							armorStand.setGravity(false);
							armorStand.setVisible(false);
							armorStand.setInvulnerable(true);
							armorStand.setBasePlate(false);
							armorStand.setSmall(true);
							armorStand.setHelmet(craftingTexture);
							armorStand.setRemoveWhenFarAway(false);
						}
						
						craftList.add(newBlock.getLocation().toString());
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerArmorStandManipulateEvent(PlayerArmorStandManipulateEvent event) {
		Entity clicked = event.getRightClicked();
		Player player = event.getPlayer();
		ItemStack craftingTexture = createTexture();
		ArmorStand armorStand = (ArmorStand) clicked;
		if (armorStand.getHelmet().isSimilar(craftingTexture)) {
			event.setCancelled(true);
			player.openWorkbench(null, true);
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		Block block = event.getBlock();
		Player player = event.getPlayer();
		if (block.getType() == Material.DROPPER) {
			if (craftList.contains(block.getLocation().toString())) {
				event.setCancelled(true);
				block.setType(Material.AIR);
				double radius = 1D;
				List<Entity> near = block.getWorld().getEntities();
				for(Entity e : near) {
					if(e.getLocation().distance(block.getLocation()) <= radius) {
						if (e instanceof ArmorStand) {
							ArmorStand armorStand = (ArmorStand) e;
							String armorLocation = armorStand.getLocation().getBlock().getLocation().toString();
							String blockLocation = block.getLocation().toString();
							if (armorLocation.equals(blockLocation)) {
								e.remove();
								break;
							}
						}
					}
				}
				ItemStack craftingTexture = createTexture();
				craftList.remove(block.getLocation().toString());
				block.getWorld().playSound(player.getLocation(), "realmsbench.break", 1, 1);
				if (player.getGameMode() != GameMode.CREATIVE) {
					block.getWorld().dropItemNaturally(block.getLocation(), craftingTexture);
				}
			}
		}
		Block belowBlock = block.getRelative(BlockFace.DOWN);
		if (craftList.contains(belowBlock.getLocation().toString())) {
			ItemStack craftingTexture = createTexture();
			ArmorStand armorStand = belowBlock.getWorld().spawn(belowBlock.getLocation().add(new Vector(0.5,0.1,0.5)), ArmorStand.class);
			armorStand.setGravity(false);
			armorStand.setVisible(false);
			armorStand.setInvulnerable(true);
			armorStand.setBasePlate(false);
			armorStand.setSmall(true);
			armorStand.setHelmet(craftingTexture);
			armorStand.setRemoveWhenFarAway(false);
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		Block block = event.getBlock();
		Block belowBlock = block.getRelative(BlockFace.DOWN);
		if (belowBlock.getType() == Material.DROPPER) {
			if (craftList.contains(belowBlock.getLocation().toString())) {
				double radius = 1D;
				List<Entity> near = block.getWorld().getEntities();
				for(Entity e : near) {
					if(e.getLocation().distance(belowBlock.getLocation()) <= radius) {
						if (e instanceof ArmorStand) {
							ArmorStand armorStand = (ArmorStand) e;
							String armorLocation = armorStand.getLocation().getBlock().getLocation().toString();
							String blockLocation = belowBlock.getLocation().toString();
							if (armorLocation.equals(blockLocation)) {
								e.remove();
								break;
							}
						}
					}
				}
			}
		}
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
    	if (cmd.getName().equalsIgnoreCase("crafter") && sender instanceof Player){
    		Player player = (Player) sender;
    		if (player.hasPermission("realms.crafter")){
    			ItemStack craftingTexture = createTexture();
    			player.getInventory().addItem(craftingTexture);
			}
    		return true;
    	}
    	return false;
    }
	
}
