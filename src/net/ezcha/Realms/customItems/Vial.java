package net.ezcha.Realms.customItems;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

import net.ezcha.Realms.main.Main;

public class Vial implements Listener, CommandExecutor {
	
	public static Server thisServer = null;
	public static Plugin thisPlugin = null;
	public static List<String> vialList = new ArrayList<String>();
	
	public static ArrayList<String> createLore(String text) {
		ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.RESET+""+ChatColor.DARK_GRAY+text);
	    return lore;
	}
	
	public int getLevelsFromItem(ItemStack item) {
		List<String> lore = item.getItemMeta().getLore();
		String string = lore.get(0);
		string = ChatColor.stripColor(string);
		string = string.replaceAll("[^\\d.]", "");
		return Integer.valueOf(string);
	}
	
	public int getLevelsFromStand(ArmorStand armorStand) {
		String string = armorStand.getCustomName();
		string = ChatColor.stripColor(string);
		string = string.replaceAll("[^\\d.]", "");
		return Integer.valueOf(string);
	}
	
	public ItemStack withoutLore(ItemStack stack) {
		ItemStack noLore = new ItemStack(stack.getType());
		noLore.setData(stack.getData());
		noLore.setItemMeta(stack.getItemMeta());
		ItemMeta noLoreMeta = noLore.getItemMeta();
		noLoreMeta.setLore(null);
		noLore.setItemMeta(noLoreMeta);
		return noLore;
	}
	
	public static ItemStack createTexture() {
		ItemStack vialTexture = new ItemStack(Material.DIAMOND_HOE);
		ItemMeta textureMeta = vialTexture.getItemMeta();
		textureMeta.spigot().setUnbreakable(true);
		textureMeta.setDisplayName(ChatColor.WHITE+"XP Vial");
		textureMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		textureMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
		textureMeta.setLore(createLore("0 Levels"));
		vialTexture.setItemMeta(textureMeta);
		vialTexture.setDurability((short)10);
		return vialTexture;
	}
	
	public Vial(Main plugin) {
		thisServer = plugin.getServer();
		thisPlugin = plugin;
		thisServer.getPluginManager().registerEvents(this, plugin);
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			Player player = event.getPlayer();
			Block block = event.getClickedBlock();
			ItemStack heldItem = player.getInventory().getItemInMainHand();
			if (block.getType() == Material.DROPPER) {
				if (vialList.contains(block.getLocation().toString())) {
					event.setCancelled(true);
					player.openWorkbench(null, true);
				}
			}
			if (block.getType().equals(Material.STAINED_GLASS)) {
				if (vialList.contains(block.getLocation().toString())) {
					if (event.getHand() == EquipmentSlot.OFF_HAND) {
				        return; // off hand packet, ignore.
				    }
					double radius = 1D;
					List<Entity> near = block.getWorld().getEntities();
					for(Entity e : near) {
						if(e.getLocation().distance(block.getLocation()) <= radius) {
							if (e instanceof ArmorStand) {
								ArmorStand armorStand = (ArmorStand) e;
								String armorLocation = armorStand.getLocation().getBlock().getLocation().toString();
								String blockLocation = block.getLocation().toString();
								if (armorLocation.equals(blockLocation)) {
									if (withoutLore(armorStand.getHelmet()).isSimilar(withoutLore(createTexture()))) {
										event.setCancelled(true);
										int levels = getLevelsFromStand(armorStand);
										if (!player.isSneaking()) {
											if (levels < 256) {
												if (player.getLevel() > 0) {
													player.setLevel(player.getLevel()-1);
													levels += 1;
													player.playSound(player.getLocation(), "xpvial.fill", 1, 1);
													if (levels != 1) {
														armorStand.setCustomName(String.valueOf(levels)+" Levels");
													} else {
														armorStand.setCustomName("1 Level");
													}
												} else {
													player.sendMessage(ChatColor.RED+"You have no experience to store.");
												}
											} else {
												player.sendMessage(ChatColor.RED+"This vial has reached maximum capacity.");
											}
										} else {
											if (levels > 0) {
												player.setLevel(player.getLevel()+1);
												levels -= 1;
												player.playSound(player.getLocation(), "xpvial.empty", 1, 1);
												if (levels != 1) {
													armorStand.setCustomName(String.valueOf(levels)+" Levels");
												} else {
													armorStand.setCustomName("1 Level");
												}
											} else {
												player.sendMessage(ChatColor.RED+"This vial is empty.");
											}
										}
									}
									break;
								}
							}
						}
					}
				}
			}
			if (heldItem.getType() == Material.DIAMOND_HOE) {
				
				ItemStack vialTexture = createTexture();
				
				if (withoutLore(heldItem).isSimilar(withoutLore(vialTexture))) {
					event.setCancelled(true);
					Block blockSide = block.getRelative(event.getBlockFace());
					int levels = getLevelsFromItem(heldItem);
					if (blockSide.getType() == Material.AIR) {
						if (player.getGameMode() != GameMode.CREATIVE) {
							player.getInventory().removeItem(heldItem);
						}
						
						blockSide.getWorld().playSound(blockSide.getLocation(), Sound.BLOCK_ENCHANTMENT_TABLE_USE, 1, 1);
						
						Block newBlock = blockSide.getWorld().getBlockAt(blockSide.getLocation());
						newBlock.setType(Material.STAINED_GLASS);
						newBlock.setData((byte)5);
						
						ArmorStand armorStand = blockSide.getWorld().spawn(blockSide.getLocation().add(new Vector(0.5,0,0.5)), ArmorStand.class);
						armorStand.setGravity(false);
						armorStand.setVisible(false);
						armorStand.setInvulnerable(true);
						armorStand.setBasePlate(false);
						armorStand.setSmall(true);
						if (levels != 1) {
							armorStand.setCustomName(String.valueOf(levels)+" Levels");
						} else {
							armorStand.setCustomName("1 Level");
						}
						armorStand.setCustomNameVisible(true);
						armorStand.setHelmet(vialTexture);
						armorStand.setRemoveWhenFarAway(false);
						
						vialList.add(newBlock.getLocation().toString());
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerArmorStandManipulateEvent(PlayerArmorStandManipulateEvent event) {
		Entity clicked = event.getRightClicked();
		Player player = event.getPlayer();
		ItemStack vialTexture = createTexture();
		ArmorStand armorStand = (ArmorStand) clicked;
		if (withoutLore(armorStand.getHelmet()).isSimilar(withoutLore(vialTexture))) {
			event.setCancelled(true);
			int levels = getLevelsFromStand(armorStand);
			if (!player.isSneaking()) {
				if (levels < 256) {
					if (player.getLevel() > 0) {
						player.setLevel(player.getLevel()-1);
						levels += 1;
						player.playSound(player.getLocation(), "xpvial.fill", 1, 1);
						if (levels != 1) {
							armorStand.setCustomName(String.valueOf(levels)+" Levels");
						} else {
							armorStand.setCustomName("1 Level");
						}
					} else {
						player.sendMessage(ChatColor.RED+"You have no experience to store.");
					}
				} else {
					player.sendMessage(ChatColor.RED+"This vial has reached maximum capacity.");
				}
			} else {
				if (levels > 0) {
					player.setLevel(player.getLevel()+1);
					levels -= 1;
					player.playSound(player.getLocation(), "xpvial.empty", 1, 1);
					if (levels != 1) {
						armorStand.setCustomName(String.valueOf(levels)+" Levels");
					} else {
						armorStand.setCustomName("1 Level");
					}
				} else {
					player.sendMessage(ChatColor.RED+"This vial is empty.");
				}
			}
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		Block block = event.getBlock();
		Player player = event.getPlayer();
		if (block.getType() == Material.STAINED_GLASS) {
			if (vialList.contains(block.getLocation().toString())) {
				event.setCancelled(true);
				int levels = 0;
				block.setType(Material.AIR);
				double radius = 1D;
				List<Entity> near = block.getWorld().getEntities();
				for(Entity e : near) {
					if(e.getLocation().distance(block.getLocation()) <= radius) {
						if (e instanceof ArmorStand) {
							ArmorStand armorStand = (ArmorStand) e;
							String armorLocation = armorStand.getLocation().getBlock().getLocation().toString();
							String blockLocation = block.getLocation().toString();
							if (armorLocation.equals(blockLocation)) {
								levels = getLevelsFromStand(armorStand);
								e.remove();
								break;
							}
						}
					}
				}
				ItemStack vialTexture = createTexture();
				vialList.remove(block.getLocation().toString());
				block.getWorld().playSound(block.getLocation(), Sound.BLOCK_ENCHANTMENT_TABLE_USE, 1, 1);
				if (!(player.getGameMode() == GameMode.CREATIVE && levels == 0)) {
					Item droppedItem = block.getWorld().dropItemNaturally(block.getLocation(), vialTexture);
					ItemStack droppedStack = droppedItem.getItemStack();
					ItemMeta droppedMeta = droppedStack.getItemMeta();
					if (levels != 1) {
						droppedMeta.setLore(createLore(String.valueOf(levels)+" Levels"));
					} else {
						droppedMeta.setLore(createLore("1 Level"));
					}
					droppedStack.setItemMeta(droppedMeta);
					droppedItem.setItemStack(droppedStack);
				}
			}
		}
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
    	if (cmd.getName().equalsIgnoreCase("vial") && sender instanceof Player){
    		Player player = (Player) sender;
    		if (player.hasPermission("realms.vial")){
    			ItemStack vialTexture = createTexture();
    			player.getInventory().addItem(vialTexture);
			}
    		return true;
    	}
    	return false;
    }
	
}
