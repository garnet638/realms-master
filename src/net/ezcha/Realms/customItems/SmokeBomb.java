package net.ezcha.Realms.customItems;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import net.ezcha.Realms.main.Main;

public class SmokeBomb implements Listener {

	public SmokeBomb(Main plugin) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	public static ItemStack createTexture() {
		ItemStack craftingTexture = new ItemStack(Material.DIAMOND_HOE);
		ItemMeta textureMeta = craftingTexture.getItemMeta();
		textureMeta.spigot().setUnbreakable(true);
		textureMeta.setDisplayName(ChatColor.WHITE+"Smoke Bomb");
		textureMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		textureMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
		craftingTexture.setItemMeta(textureMeta);
		craftingTexture.setDurability((short)11);
		return craftingTexture;
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			Player player = event.getPlayer();
			PlayerInventory inventory = player.getInventory();
			Block block = event.getClickedBlock();
			if (inventory.getItemInMainHand().isSimilar(createTexture())) {
				if (player.getGameMode() != GameMode.CREATIVE) {
					inventory.removeItem(inventory.getItemInMainHand());
				}
				event.setCancelled(true);
				Entity entity = player.getWorld().spawnEntity(block.getLocation().add(new Vector(0.5,1,0.5)), EntityType.ENDERMITE);
				player.getWorld().playSound(entity.getLocation(), "smokebomb.detonate", 1, 1);
				entity.setCustomName("SmokeBomb");
				entity.setCustomNameVisible(false);
				entity.setGravity(false);
				entity.setSilent(true);
			}
		}
	}
	
}
