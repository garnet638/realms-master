package net.ezcha.Realms.customItems;

import java.util.ArrayList;
import java.util.HashMap;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import net.ezcha.Realms.main.Main;
import net.md_5.bungee.api.ChatColor;

public class Recipes implements Listener {

	public static Server thisServer = null;
	public static Plugin thisPlugin = null;
	public static HashMap<Player, Block> selectedBench = new HashMap<Player, Block>();
	public static ArrayList<ItemStack> exclusiveRecipes = new ArrayList<ItemStack>();
	
	public ItemStack createGlider() {
		ItemStack item = new ItemStack(Material.ELYTRA);
		ItemMeta itemMeta = item.getItemMeta();
		itemMeta.setDisplayName(ChatColor.WHITE+"Glider");
		itemMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
		itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		item.setItemMeta(itemMeta);
		return item;
	}
	
	public ItemStack createTreeFeller() {
		ItemStack item = new ItemStack(Material.IRON_AXE);
		ItemMeta itemMeta = item.getItemMeta();
		itemMeta.setDisplayName(ChatColor.WHITE+"Tree Feller");
		itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
		item.setItemMeta(itemMeta);
		return item;
	}
	
	public Recipes(Main plugin) {
		thisServer = plugin.getServer();
		thisPlugin = plugin;
		thisServer.getPluginManager().registerEvents(this, plugin);
		createRecipes();
	}
	
	public void createRecipes() {
		/* Realms Workbench */
		ItemStack benchStack = CraftingBlock.createTexture();
		ShapedRecipe bench = new ShapedRecipe(benchStack);
		bench.shape("sps","oco","ttt");
		bench.setIngredient('s', Material.STONE);
		bench.setIngredient('p', Material.ENDER_PEARL);
		bench.setIngredient('o', Material.OBSIDIAN);
		bench.setIngredient('t', Material.COBBLESTONE);
		bench.setIngredient('c', Material.WORKBENCH);
		thisServer.addRecipe(bench);
		
		/* Vial */
		ItemStack vialStack = Vial.createTexture();
		ShapedRecipe vial = new ShapedRecipe(vialStack);
		vial.shape("geg","gpg","geg");
		vial.setIngredient('g', Material.GLASS);
		vial.setIngredient('e', Material.EMERALD);
		vial.setIngredient('p', Material.ENDER_PEARL);
		exclusiveRecipes.add(vialStack);
		thisServer.addRecipe(vial);
		
		/* Glider */
		ItemStack gliderStack = createGlider();
		ShapedRecipe glider = new ShapedRecipe(gliderStack);
		glider.shape("www","ses","fff");
		glider.setIngredient('w', Material.WOOL);
		glider.setIngredient('s', Material.STRING);
		glider.setIngredient('e', Material.ELYTRA);
		glider.setIngredient('f', Material.FEATHER);
		exclusiveRecipes.add(gliderStack);
		thisServer.addRecipe(glider);
		
		/* Tree Feller */
		ItemStack treeStack = createTreeFeller();
		ShapedRecipe tree = new ShapedRecipe(treeStack);
		tree.shape("iqa","qsa","asa");
		tree.setIngredient('i', Material.IRON_BLOCK);
		tree.setIngredient('q', Material.QUARTZ_BLOCK);
		tree.setIngredient('s', Material.STICK);
		exclusiveRecipes.add(treeStack);
		thisServer.addRecipe(tree);
		
		/* Smoke Bomb */
		ItemStack bombStack = SmokeBomb.createTexture();
		ShapelessRecipe bomb = new ShapelessRecipe(bombStack);
		bomb.addIngredient(Material.SULPHUR);
		bomb.addIngredient(Material.STRING);
		bomb.addIngredient(2,Material.INK_SACK);
		exclusiveRecipes.add(bombStack);
		thisServer.addRecipe(bomb);
		
		/* Packed Ice */
		ItemStack iceStack = new ItemStack(Material.PACKED_ICE,4);
		ShapedRecipe ice = new ShapedRecipe(iceStack);
		ice.shape("iii","iii","iii");
		ice.setIngredient('i', Material.ICE);
		exclusiveRecipes.add(iceStack);
		thisServer.addRecipe(ice);
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			Block block = event.getClickedBlock();
			Player player = event.getPlayer();
			if (block.getType() == Material.DROPPER) {
				if (CraftingBlock.craftList.contains(block.getLocation().toString())) {
					selectedBench.put(player, block);
				}
			}
			if (block.getType() == Material.WORKBENCH) {
				selectedBench.put(player, block);
			}
		}
	}
	
	@EventHandler
	public void  onCraftPrepare(PrepareItemCraftEvent event) {
		if (event.getInventory() instanceof CraftingInventory) {
			Player player = (Player) event.getViewers().get(0);
			if (selectedBench.get(player) != null) {
				if (!(CraftingBlock.craftList.contains(selectedBench.get(player).getLocation().toString()))) {
					CraftingInventory inventory = (CraftingInventory) event.getInventory();
					if (inventory.getResult() != null) {
						//Vial
						if (exclusiveRecipes.contains(inventory.getResult())) {
							inventory.setResult(null);
						}
					}
				}
			}
		}
	}
	
}
