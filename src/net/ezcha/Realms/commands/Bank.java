package net.ezcha.Realms.commands;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.SkullType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import net.ezcha.Realms.main.Main;
import net.milkbowl.vault.economy.Economy;

public class Bank implements Listener, CommandExecutor {
	
	//Register that it's a plugin
	Main plugin;
	HashMap<Player, Player> playerReciever = new HashMap<Player, Player>();
	public static List<HashMap<String, String>> shopsList = new ArrayList<HashMap<String, String>>();
	public static Economy econ = null;
	public static NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
	public static Server thisServer;
	ArrayList<String> playerInputName = new ArrayList<String>();
	ArrayList<String> playerInputWarp = new ArrayList<String>();
	HashMap<String, String> shopCreateName = new HashMap<String, String>();
	HashMap<String, String> shopCreateWarp = new HashMap<String, String>();
	
	public static void addShop(String name, String warp, Material type) {
		HashMap<String, String> shop = new HashMap<String, String>();
		shop.put("Name", name);
		shop.put("Warp", warp);
		shop.put("Icon", type.toString());
		shopsList.add(shop);
	}
	
	public static ArrayList<String> createLore(String text) {
		ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.DARK_GRAY+text);
	    return lore;
	}
	
	public Bank(Main plugin){
        this.plugin = plugin; 
        thisServer = plugin.getServer();
        econ = Main.econ;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	public void createATM(Player player) {
		Inventory inventory = Bukkit.createInventory(null, 9, ChatColor.BLUE+"Bank");
		
		//Balance
		ItemStack bal = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());
	    SkullMeta balM = (SkullMeta) bal.getItemMeta();
	    balM.setOwner(player.getName());
	    balM.setDisplayName(ChatColor.GREEN+"Balance");
	    String balance = currencyFormatter.format(Double.valueOf(econ.getBalance(player)));
	    balM.setLore(createLore(balance));
	    bal.setItemMeta(balM);
		
	    //Transfer
	    ItemStack transfer = new ItemStack(Material.ARROW);
	    ItemMeta transferM = transfer.getItemMeta();
	    transferM.setDisplayName(ChatColor.GREEN+"Transfer");
	    transferM.setLore(createLore("Transfers funds to another player."));
	    transfer.setItemMeta(transferM);
	    
	    //Shops
	    ItemStack shopList = new ItemStack(Material.BOOK);
	    ItemMeta shopListM = transfer.getItemMeta();
	    shopListM.setDisplayName(ChatColor.GREEN+"Shops");
	    shopListM.setLore(createLore("View the shop list."));
	    shopList.setItemMeta(shopListM);
	    
	    inventory.setItem(0,bal);
	    inventory.setItem(1,transfer);
	    inventory.setItem(2,shopList);
	    
		player.openInventory(inventory);
	}
	
	public void createTransfer(Player player) {
		int playerNum = 0;
		Inventory inventory = Bukkit.createInventory(null, 54, ChatColor.BLUE+"Transfer");
		
		for (Player loopPlayer : thisServer.getOnlinePlayers()) {
			if (!loopPlayer.getName().equals(player.getName())) {
				ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());
			    SkullMeta headM = (SkullMeta) head.getItemMeta();
			    headM.setOwner(loopPlayer.getName());
			    headM.setDisplayName(loopPlayer.getDisplayName());
			    headM.setLore(createLore(ChatColor.DARK_GRAY+"("+loopPlayer.getName()+")"));
			    headM.addEnchant(Enchantment.THORNS, 1, true);
			    headM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			    head.setItemMeta(headM);
			    inventory.setItem(playerNum,head);
			    playerNum += 1;
			}
			ItemStack back = new ItemStack(Material.BARRIER);
			ItemMeta backM = back.getItemMeta();
			backM.setDisplayName(ChatColor.RED+"Back");
			backM.setLore(createLore("Return to the previous menu."));
			back.setItemMeta(backM);
			inventory.setItem(53,back);
		}
	    
		player.openInventory(inventory);
	}
	
	public void createSendTransfer(Player send, Player get) {
		Inventory inventory = Bukkit.createInventory(null, 9, ChatColor.BLUE+"Send Transfer");
		
		ItemStack back = new ItemStack(Material.BARRIER);
		ItemMeta backM = back.getItemMeta();
		backM.setDisplayName(ChatColor.RED+"Back");
		backM.setLore(createLore("Return to the previous menu."));
		back.setItemMeta(backM);
	    
		ItemStack sendCoal = new ItemStack(Material.COAL);
		ItemMeta sendCoalM = sendCoal.getItemMeta();
		sendCoalM.setDisplayName(ChatColor.GREEN+"$1");
		sendCoal.setItemMeta(sendCoalM);
		
		ItemStack sendRedstone = new ItemStack(Material.REDSTONE);
		ItemMeta sendRedstoneM = sendRedstone.getItemMeta();
		sendRedstoneM.setDisplayName(ChatColor.GREEN+"$5");
		sendRedstone.setItemMeta(sendRedstoneM);
		
		ItemStack sendIron = new ItemStack(Material.IRON_INGOT);
		ItemMeta sendIronM = sendIron.getItemMeta();
		sendIronM.setDisplayName(ChatColor.GREEN+"$10");
		sendIron.setItemMeta(sendIronM);
		
		ItemStack sendGold = new ItemStack(Material.GOLD_INGOT);
		ItemMeta sendGoldM = sendGold.getItemMeta();
		sendGoldM.setDisplayName(ChatColor.GREEN+"$25");
		sendGold.setItemMeta(sendGoldM);
		
		ItemStack sendDiamond = new ItemStack(Material.DIAMOND);
		ItemMeta sendDiamondM = sendDiamond.getItemMeta();
		sendDiamondM.setDisplayName(ChatColor.GREEN+"$50");
		sendDiamond.setItemMeta(sendDiamondM);
		
		ItemStack sendEmerald = new ItemStack(Material.EMERALD);
		ItemMeta sendEmeraldM = sendEmerald.getItemMeta();
		sendEmeraldM.setDisplayName(ChatColor.GREEN+"$100");
		sendEmerald.setItemMeta(sendEmeraldM);
		
		inventory.setItem(0,sendCoal);
		inventory.setItem(1,sendRedstone);
		inventory.setItem(2,sendIron);
		inventory.setItem(3,sendGold);
		inventory.setItem(4,sendDiamond);
		inventory.setItem(5,sendEmerald);
		inventory.setItem(8,back);
		
		send.openInventory(inventory);
	}
	
	public void createShops(Player player) {
		Inventory inventory = Bukkit.createInventory(null, 54, ChatColor.BLUE+"Shops");

		for (int i = 0; i < shopsList.size(); i++) {
			HashMap<String, String> map = shopsList.get(i);
			ItemStack icon = new ItemStack(Material.valueOf(map.get("Icon")));
			ItemMeta iconMeta = icon.getItemMeta();
			iconMeta.setDisplayName(ChatColor.WHITE+map.get("Name"));
			iconMeta.setLore(createLore("/warp "+map.get("Warp")));
			iconMeta.addEnchant(Enchantment.THORNS, 1, true);
			iconMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
			iconMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			icon.setItemMeta(iconMeta);
			inventory.setItem(i, icon);
		}
		
		ItemStack back = new ItemStack(Material.BARRIER);
		ItemMeta backM = back.getItemMeta();
		backM.setDisplayName(ChatColor.RED+"Back");
		backM.setLore(createLore("Return to the previous menu."));
		back.setItemMeta(backM);
		inventory.setItem(53,back);
		
		ItemStack add = new ItemStack(Material.BOOK_AND_QUILL);
		ItemMeta addM = add.getItemMeta();
		addM.setDisplayName(ChatColor.GREEN+"Add Shop");
		addM.setLore(createLore("Add a shop to this list."));
		add.setItemMeta(addM);
		inventory.setItem(52,add);
	    
		player.openInventory(inventory);
	}
	
	public static Inventory iconSelectMenu = Bukkit.createInventory(null, 36, ChatColor.BLUE+"Icons");
	static {
		iconSelectMenu.setItem(0, new ItemStack(Material.COBBLESTONE));
		iconSelectMenu.setItem(1, new ItemStack(Material.WOOD));
		iconSelectMenu.setItem(2, new ItemStack(Material.LOG));
		iconSelectMenu.setItem(3, new ItemStack(Material.BOOKSHELF));
		iconSelectMenu.setItem(4, new ItemStack(Material.GRASS));
		iconSelectMenu.setItem(5, new ItemStack(Material.SMOOTH_BRICK));
		iconSelectMenu.setItem(6, new ItemStack(Material.DIAMOND_ORE));
		iconSelectMenu.setItem(7, new ItemStack(Material.SANDSTONE));
		iconSelectMenu.setItem(8, new ItemStack(Material.ENDER_STONE));
		iconSelectMenu.setItem(9, new ItemStack(Material.NETHERRACK));
		iconSelectMenu.setItem(10, new ItemStack(Material.BRICK));
		iconSelectMenu.setItem(11, new ItemStack(Material.ICE));
		iconSelectMenu.setItem(12, new ItemStack(Material.JACK_O_LANTERN));
		iconSelectMenu.setItem(13, new ItemStack(Material.SPONGE));
		iconSelectMenu.setItem(14, new ItemStack(Material.OBSIDIAN));
		iconSelectMenu.setItem(15, new ItemStack(Material.DRAGON_EGG));
		iconSelectMenu.setItem(16, new ItemStack(Material.ENCHANTMENT_TABLE));
		iconSelectMenu.setItem(17, new ItemStack(Material.SNOW_BLOCK));
		iconSelectMenu.setItem(18, new ItemStack(Material.BOOK));
		iconSelectMenu.setItem(19, new ItemStack(Material.CAKE));
		iconSelectMenu.setItem(20, new ItemStack(Material.BREAD));
		iconSelectMenu.setItem(21, new ItemStack(Material.BAKED_POTATO));
		iconSelectMenu.setItem(22, new ItemStack(Material.DIAMOND_SWORD));
		iconSelectMenu.setItem(23, new ItemStack(Material.IRON_AXE));
		iconSelectMenu.setItem(24, new ItemStack(Material.GOLD_PICKAXE));
		iconSelectMenu.setItem(25, new ItemStack(Material.WOOD_HOE));
		iconSelectMenu.setItem(26, new ItemStack(Material.LEATHER_HELMET));
		iconSelectMenu.setItem(27, new ItemStack(Material.CHAINMAIL_CHESTPLATE));
		iconSelectMenu.setItem(28, new ItemStack(Material.DIAMOND));
		iconSelectMenu.setItem(29, new ItemStack(Material.EMERALD));
		iconSelectMenu.setItem(30, new ItemStack(Material.GOLD_INGOT));
		iconSelectMenu.setItem(31, new ItemStack(Material.IRON_INGOT));
		iconSelectMenu.setItem(32, new ItemStack(Material.REDSTONE));
		iconSelectMenu.setItem(33, new ItemStack(Material.COAL));
		iconSelectMenu.setItem(34, new ItemStack(Material.BREWING_STAND_ITEM));
		iconSelectMenu.setItem(35, new ItemStack(Material.DRAGONS_BREATH));
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
    	if (cmd.getName().equalsIgnoreCase("bank") && sender instanceof Player){
    		Player player = (Player) sender;
    		if (player.hasPermission("realms.bank")) {
    			if (args.length != 1) {
    				createATM(player);
    			} else {
    				if (args[0].equals("transfer")) {
    					createTransfer(player);
    				} else if (args[0].equals("shops")) {
    					createShops(player);
    				} else {
    					createATM(player);
    				}
    			}
			}
    		return true;
    	}
    	return false;
    }
	
	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent event) {
		Player player = event.getPlayer();
		if (player.getOpenInventory().getTitle().equals(ChatColor.BLUE+"Icons") || player.getOpenInventory().getTitle().equals(ChatColor.BLUE+"Bank") || player.getOpenInventory().getTitle().equals(ChatColor.BLUE+"Transfer") || player.getOpenInventory().getTitle().equals(ChatColor.BLUE+"Shops")) {
			event.setCancelled(true);
			player.updateInventory();
		}
	}
	
	@EventHandler
	public void onInventoryClosed(InventoryCloseEvent event) {
		Inventory inventory = event.getInventory();
		Player player = (Player) event.getPlayer();
		if (inventory.getTitle().equals(ChatColor.BLUE+"Icons")) {
			if (shopCreateName.containsKey(player.getName()) && shopCreateWarp.containsKey(player.getName())) {
				shopCreateName.remove(player.getName());
				shopCreateWarp.remove(player.getName());
				player.sendMessage(ChatColor.RED+"Canceled shop listing.");
			}
		}
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
	    ItemStack clicked = event.getCurrentItem();
	    Inventory inventory = event.getInventory();
	    if (inventory.getName().equals(ChatColor.BLUE+"Icons")) {
	    	event.setCancelled(true);
	    	if (shopsList.size() >= 52) {
	    		shopsList.remove(0);
	    	}
	    	player.sendMessage(ChatColor.YELLOW+"Shop icon set.");
	    	addShop(shopCreateName.get(player.getName()), shopCreateWarp.get(player.getName()), clicked.getType());
	    	shopCreateName.remove(player.getName());
			shopCreateWarp.remove(player.getName());
			createShops(player);
			player.sendMessage(ChatColor.GREEN+"Your shop has been added to the list.");
	    }
	    if (inventory.getName().equals(ChatColor.BLUE+"Bank")) {
	    	event.setCancelled(true);
	    	if (clicked.getType() == Material.ARROW) {
	    		createTransfer(player);
	    	}
	    	if (clicked.getType() == Material.BOOK) {
	    		createShops(player);
	    	}
	    }
	    if (inventory.getName().equals(ChatColor.BLUE+"Transfer")) {
	    	event.setCancelled(true);
	    	if (clicked.getType() == Material.BARRIER) {
	    		createATM(player);
	    	}
	    	if (clicked.getType() == Material.SKULL_ITEM) {
			    SkullMeta clickedM = (SkullMeta) clicked.getItemMeta();
			    playerReciever.put(player, thisServer.getPlayer(clickedM.getOwner()));
	    		createSendTransfer(player,thisServer.getPlayer(clickedM.getOwner()));
	    	}
	    }
	    if (inventory.getName().equals(ChatColor.BLUE+"Send Transfer")) {
	    	event.setCancelled(true);
	    	if (clicked.getType() == Material.BARRIER) {
	    		createTransfer(player);
	    	}
	    	if (clicked.getType() == Material.COAL) {
	    		player.chat("/pay "+playerReciever.get(player).getName()+" 1");
	    		playerReciever.remove(player);
	    		player.closeInventory();
	    	}
	    	if (clicked.getType() == Material.REDSTONE) {
	    		player.chat("/pay "+playerReciever.get(player).getName()+" 5");
	    		playerReciever.remove(player);
	    		player.closeInventory();
	    	}
	    	if (clicked.getType() == Material.IRON_INGOT) {
	    		player.chat("/pay "+playerReciever.get(player).getName()+" 10");
	    		playerReciever.remove(player);
	    		player.closeInventory();
	    	}
	    	if (clicked.getType() == Material.GOLD_INGOT) {
	    		player.chat("/pay "+playerReciever.get(player).getName()+" 25");
	    		playerReciever.remove(player);
	    		player.closeInventory();
	    	}
	    	if (clicked.getType() == Material.DIAMOND) {
	    		player.chat("/pay "+playerReciever.get(player).getName()+" 50");
	    		playerReciever.remove(player);
	    		player.closeInventory();
	    	}
	    	if (clicked.getType() == Material.EMERALD) {
	    		player.chat("/pay "+playerReciever.get(player).getName()+" 100");
	    		playerReciever.remove(player);
	    		player.closeInventory();
	    	}
	    }
	    if (inventory.getName().equals(ChatColor.BLUE+"Shops")) {
	    	event.setCancelled(true);
	    	if (clicked.getType() == Material.BARRIER) {
	    		createATM(player);
	    	} else if (clicked.getType() == Material.BOOK_AND_QUILL) {
	    		player.closeInventory();
	    		playerInputName.add(player.getName());
	    		player.sendMessage(ChatColor.GREEN+"Send the shop's name as a chat message. Type 'cancel' to cancel this.");
	    	} else {
	    		player.chat(ChatColor.stripColor(clicked.getItemMeta().getLore().get(0)));
	    		player.closeInventory();
	    	}
	    }
	}
	
	@EventHandler
	public void playerChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		String message = event.getMessage();
		if (playerInputWarp.contains(player.getName())) {
			event.setCancelled(true);
			playerInputWarp.remove(player.getName());
			if (message.equalsIgnoreCase("cancel")) {
				shopCreateName.remove(player.getName());
				player.sendMessage(ChatColor.RED+"Canceled shop listing.");
			} else {
				if (message.startsWith("/warp ")) {
					message = message.substring(6);
				}
				shopCreateWarp.put(player.getName(), message);
				player.sendMessage(ChatColor.YELLOW+"Warp set as \""+message+"\"");
				player.sendMessage(ChatColor.GREEN+"Select the shop's icon. Close the window to cancel this.");
				player.openInventory(iconSelectMenu);
			}
		}
		if (playerInputName.contains(player.getName())) {
			event.setCancelled(true);
			playerInputName.remove(player.getName());
			if (message.equalsIgnoreCase("cancel")) {
				player.sendMessage(ChatColor.RED+"Canceled shop listing.");
			} else {
				playerInputWarp.add(player.getName());
				shopCreateName.put(player.getName(), message);
				player.sendMessage(ChatColor.YELLOW+"Shop name set as \""+message+"\"");
				player.sendMessage(ChatColor.GREEN+"Type in the warp's name. Type 'cancel' to cancel this.");
			}
		}
	}
	
}