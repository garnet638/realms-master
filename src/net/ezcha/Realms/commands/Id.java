package net.ezcha.Realms.commands;

import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.ezcha.Realms.main.Main;

public class Id implements CommandExecutor {

	public Server thisPlugin() {
		Main plugin = Main.instance;
		return plugin.getServer();
	}
	
	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("id") && sender instanceof Player) {
    		Player player = (Player) sender;
    		player.sendMessage("The id of "+String.valueOf(player.getInventory().getItemInMainHand().getType())+" is "+String.valueOf(player.getInventory().getItemInMainHand().getTypeId())+".");
    		return true;
    	}
    	return false;
	}
	
}
