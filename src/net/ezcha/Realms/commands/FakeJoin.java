package net.ezcha.Realms.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FakeJoin implements CommandExecutor{

public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		
		if (cmd.getName().equalsIgnoreCase("fakejoin")){
			
			Player player = (Player) sender;
			String playername = ((Player) sender).getName();
			int length = args.length;
			if (player.hasPermission("realms.mod.fakejoin")){
				
				if (length == 0)
				{
					for(Player p : Bukkit.getOnlinePlayers()) {
						p.sendMessage(ChatColor.LIGHT_PURPLE+playername+ChatColor.AQUA+" has joined Mine-Imator Realms.");
						if (p.hasPermission("realms.mod.fakejoin"))
						{
							p.sendMessage(ChatColor.RED+"That was a fake join message by "+playername);
						}	
					}
				}
				
				if (length == 1)
				{
					for(Player p : Bukkit.getOnlinePlayers()) {
						p.sendMessage(ChatColor.LIGHT_PURPLE+args[0]+ChatColor.AQUA+" has joined Mine-Imator Realms.");
						if (p.hasPermission("realms.mod.fakejoin"))
						{
							p.sendMessage(ChatColor.RED+"That was a fake join message by "+playername);
						}	
					}
				}
				
				return true;
			}
			else if (!player.hasPermission("realms.mod.fakejoin")){
				player.sendMessage(ChatColor.RED+"You do not have permission to do that command.");
			}
		}
			return false;
	}	
}
