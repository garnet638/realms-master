package net.ezcha.Realms.commands;

import net.ezcha.Realms.main.Broadcast;
import net.ezcha.Realms.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ToggleInfo implements CommandExecutor {

public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		
		if (cmd.getName().equalsIgnoreCase("info") && sender instanceof Player) {
			
			Player player = (Player) sender;
			
			if (!Broadcast.ignoreList.contains(player.getUniqueId().toString())) {
				player.sendMessage(ChatColor.RED+"Info broadcasts disabled.");
				Broadcast.ignoreList.add(player.getUniqueId().toString());
				Main.instance.saveConfigData();
			} else {
				player.sendMessage(ChatColor.GREEN+"Info broadcasts enabled.");
				Broadcast.ignoreList.remove(player.getUniqueId().toString());
				Main.instance.saveConfigData();
			}
			
			return true;
		}
		return false;
	}	
}