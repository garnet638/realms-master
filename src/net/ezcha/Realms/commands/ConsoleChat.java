package net.ezcha.Realms.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ConsoleChat implements CommandExecutor{

public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		
		if (cmd.getName().equalsIgnoreCase("sc")){
			
			Player player = (Player) sender;
			String msg = "";
			for (int i = 0; i < args.length; i++) {
				msg += args[i] + ' ';
			}
			if (player.hasPermission("realms.mod.sc")){
				for(Player p : Bukkit.getOnlinePlayers()) {
					p.sendMessage("[Server] "+msg);
				}
				return true;
			}
			else if (!player.hasPermission("realms.mod.sc")){
				player.sendMessage(ChatColor.RED+"You do not have permission to do that command.");
			}
		}
			return false;
	}	
}
