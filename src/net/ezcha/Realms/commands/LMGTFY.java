package net.ezcha.Realms.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.bukkit.ChatColor;

public class LMGTFY implements CommandExecutor {

	/* http://lmgtfy.com/?q= */
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length != 0) {
			String link = "";
			for (int i = 0; i < args.length; i ++) {
				link += args[i];
				if (i != args.length-1) {
					link += " ";
				}
			}
			try {
				link = URLEncoder.encode(link, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				sender.sendMessage(ChatColor.RED+"Something went wrong.");
				return true;
			}
			link = "http://lmgtfy.com/?q="+link;
			sender.sendMessage(ChatColor.GREEN+"Link created. "+ChatColor.BLUE+link);
			return true;
		} else {
			sender.sendMessage(ChatColor.RED+"Incorrect usage. /lmgtfy [search phrase]");
		}
		return false;
	}
	
}
