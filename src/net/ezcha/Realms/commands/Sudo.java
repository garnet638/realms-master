package net.ezcha.Realms.commands;

import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.ezcha.Realms.main.Main;

public class Sudo implements CommandExecutor {

	public Server thisPlugin() {
		Main plugin = Main.instance;
		return plugin.getServer();
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if (cmd.getName().equalsIgnoreCase("sudo") && sender instanceof Player) {
    		Player player = (Player) sender;
    		if (player.hasPermission("realms.sudo")) {
    			if (args.length < 2) {
    				player.sendMessage(ChatColor.RED + "Invalid syntax." + ChatColor.BOLD + " /sudo [username] [message/command]");
    				return true;
    			} else {
    				Player doSudo = thisPlugin().getPlayer(args[0]);
    				if (doSudo != null) {
    					if (doSudo.hasPermission("realms.nosudo")) {
    						player.sendMessage(ChatColor.RED + "You cannot sudo that user.");
    						return true;
    					} else {
    						String sudoCommand = "";
    						for(int i = 1; i < args.length; i++) {
    							sudoCommand += args[i];
    							if (i != args.length-1) {
    								sudoCommand += " ";
    							}
    						}
    						player.sendMessage(ChatColor.GREEN + "Running \"" + ChatColor.WHITE + sudoCommand + ChatColor.GREEN + "\" on " + ChatColor.WHITE + doSudo.getDisplayName() + ChatColor.GREEN + ".");
    						doSudo.chat(sudoCommand);
    						return true;
    					}
    				} else {
    					player.sendMessage(ChatColor.RED + "Player not found.");
    					return true;
    				}
    			}
    		} else {
    			player.sendMessage(ChatColor.RED + "You do not have permsission to use this command.");
    		}
    	}
    	return false;
	}
	
}
