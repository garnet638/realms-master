package net.ezcha.Realms.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AdminChat implements CommandExecutor{

public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		
		if (cmd.getName().equalsIgnoreCase("a")){
			
			Player player = (Player) sender;
			String playername = ((Player) sender).getName();
			if (player.hasPermission("realms.mod.a")){
				
				String msg = "";
				for (int i = 0; i < args.length; i++) {
					msg += args[i] + ' ';
				}
				
				for(Player p : Bukkit.getOnlinePlayers()) {
					if (p.hasPermission("realms.mod.a"))
					{
						msg = ChatColor.translateAlternateColorCodes('&', msg);
						p.sendMessage(ChatColor.DARK_RED+"["+ChatColor.RED+"A"+ChatColor.DARK_RED+"] "+ChatColor.RED+playername+": "+ChatColor.WHITE+msg);
					}
				}
				
				return true;
			}
			else if (!player.hasPermission("realms.mod.a")){
				player.sendMessage(ChatColor.RED+"You do not have permission to do that command.");
			}
		}
			return false;
	}	
}
