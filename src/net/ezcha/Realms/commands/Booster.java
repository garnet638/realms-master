package net.ezcha.Realms.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.ezcha.Realms.main.BoostEffects;
import net.ezcha.Realms.main.Main;

public class Booster implements CommandExecutor {

	boolean doCommand = false;
	
	public Server thisPlugin() {
		Main plugin = Main.instance;
		return plugin.getServer();
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("boost")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				if (player.hasPermission("realms.boost")) {
					doCommand = true;
				}
			} else {
				doCommand = true;
			}
			if (doCommand) {
				String chatTemplate = ChatColor.DARK_PURPLE+"["+ChatColor.LIGHT_PURPLE+"Shop"+ChatColor.DARK_PURPLE+"] "+ChatColor.WHITE;
				thisPlugin().broadcastMessage(chatTemplate+args[1]+" boost (x"+args[2]+") for "+args[3]+" minutes provided by "+args[0]+".");
				
				for (@SuppressWarnings("unused") Player p : Bukkit.getOnlinePlayers()) {
					BoostEffects.boost = args[1];
					BoostEffects.boostMul = Integer.parseInt(args[2]);
					BoostEffects.boostTime = Integer.parseInt(args[3])*60*20;
				}
			}
			
			if (BoostEffects.boostTime > 0){
				for (Player p : Bukkit.getOnlinePlayers()) {
					//Check if they have the right shit
					if (BoostEffects.boost == "XP")
					{
						p.sendMessage("top kek " + BoostEffects.boostTime);
						
						BoostEffects.boostTime-=1;
					}
				 }
			}
			
    		return true;
    	}
    	return false;
	}
	
}
