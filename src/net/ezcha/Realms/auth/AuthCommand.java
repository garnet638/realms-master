package net.ezcha.Realms.auth;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class AuthCommand implements CommandExecutor {
	
	//Listen for commands
		public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
			
			if (cmd.getName().equalsIgnoreCase("login")) {
				Player player = (Player) sender;
				
				if (!player.hasPermission("realms.auth")) {
					if (args.length != 1) {
						player.sendMessage(ChatColor.RED + "Invalid syntax." + ChatColor.BOLD + " /verify [forums username]");
						return true;
					} else {
						String response = "";
						//String response = getHTML("http://forums.ezcha.net/api/getUsername.php?u="+args[0]);
						if (response  == "undefined") {
							player.sendMessage(ChatColor.RED + "You have not set your Minecraft username in your forums profile.");
						}
						if (response == "error") {
							player.sendMessage(ChatColor.RED + "We could not verify your info. Make sure there are no typos in your username.");
						}
						if (response != "error" && response != "undefined") {
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "pex user "+player.getName()+" group set Member");
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "nick "+player.getName()+" "+args[0]);
							player.sendMessage(ChatColor.GREEN + "You have sucessfully logged in.");
						}
					}
				} else {
					player.sendMessage(ChatColor.RED + "You are already logged in.");
				}
				
			}
			
			return true;
		}
	
}
