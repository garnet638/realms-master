package net.ezcha.Realms.auth;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import net.ezcha.Realms.main.Main;

public class Auth implements Listener {
	
	public Auth(Main plugin) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	//Set event cancel feedback message
	public void sendErrorMessage(Player player) {
		player.sendMessage(ChatColor.RED + "You must login before playing!");
		player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "/login <username> <password>");
	}
	
	
	//Cancel Events
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		
		if (!player.hasPermission("realms.auth")) {
			event.setCancelled(true);
			sendErrorMessage(player);
		}
		
	}
	
	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent event){
		Player player = event.getPlayer();
		
		if (!player.hasPermission("realms.auth")) {
			event.setCancelled(true);
			sendErrorMessage(player);
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		
		if (!player.hasPermission("realms.auth")) {
			event.setCancelled(true);
			sendErrorMessage(player);
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		
		if (!player.hasPermission("realms.auth")) {
			event.setCancelled(true);
			sendErrorMessage(player);
		}
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		
		if (!player.hasPermission("realms.auth")) {
			event.setCancelled(true);
			sendErrorMessage(player);
		}
	}
	
}
